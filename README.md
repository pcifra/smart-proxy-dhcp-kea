# SmartProxyDhcpKea

This Foreman smart proxy plugin allow to interface with Kea DHCP provider.

# Installation 

Smart proxy plugin are ruby gems. They can be easly instaled with `gem`. 

## Prerequisites

The smart proxy need to interact with mysql db. To do that it uses `mysql2` gem, which are automatically installed, but some dependencies are needed. So if gem installation goes wrong, try to install them with `yum`:

	yum install ruby-devel
	yum install mysql-devel

## Manual installation

It's possible to install manually this smart proxy plugin using `gem`
	
	gem install 'smart_proxy_dhcp_kea'

The gem with all its dependencies will be installed. Next you need to specify the gem into the `~foreman-proxy/bundler.d/Gemfile.local.rb` file. Create the file if it doesn't exist.

	echo "gem 'smat_proxy_dhcp_kea'" > /usr/share/foreman-proxy/bundler.d/Gemfile.local.rb

Next restart the foreman-proxy service and refresh features form the control panel.

	systemctl restart foreman-proxy  

The DHCP feature should be now active. 

# Configuration

To enable this DHCP provider, edit `/etc/foreman-proxy/settings.d/dhcp.yml` and set:

	:use_provider: dhcp_kea

Then you need to create `/etc/foreman-proxy/settings.d/dhcp_kea.yml` file and specify the following parameters.

	#KEA database name
	:dbname: [database_name]

	#KEA database username
	:username: [username]

	#KEA database password
	:password: [password]

	#KEA database ip address
	:host: [host_address]

	#KEA databse port
	:port: [db_port]

# TO DO
The smart proxy write directly on the database. It can be updated in order to use the KEA API REST.

# Contributing  
Pierfrancesco Cifra

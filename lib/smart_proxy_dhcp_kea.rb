module Proxy
  module DHCP
    module DhcpKea; end
  end
end

require 'smart_proxy_dhcp_kea/version'
require 'smart_proxy_dhcp_kea/plugin_configuration'
require 'smart_proxy_dhcp_kea/dhcp_kea_plugin'

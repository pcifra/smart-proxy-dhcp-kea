require 'dhcp_common/dhcp_common'
require 'dhcp_common/server'


module Proxy::DHCP::DhcpKea
  class Provider < ::Proxy::DHCP::Server
    attr_reader :host, :port, :dbname, :username, :password, :dhcp_kea_network, :subnet_service, :free_ips, :db

    def initialize(host, port, dbname, username, password, dhcp_kea_network, subnet_service, free_ip_service, db)
      @host = host
      @port = port
      @db = db
      @dbname = dbname
      @username = username
      @password = password
      @dhcp_kea_network = dhcp_kea_network
      @subnet_service = subnet_service
      @free_ips = free_ip_service
      super('kea', 'nil', subnet_service, free_ips)
    end

    def unused_ip(subnet_address, mac_address, from_address, to_address)
      require 'ipaddr'
      require 'resolv' 
      require 'mysql2'
  
      con = Mysql2::Client.new(:dbname => dbname, :user => username, :password => password, :host => host, :port => port) 
      st = con.prepare("select subnet_prefix from dhcp4_subnet where subnet_prefix like ?;")
      subnet = st.execute('%' + subnet_address + '%')

      if subnet.count.zero?
        logger.warning 'This subnet does not exist. Not found in databse'
        return nil
      end

      mask = '/' + subnet.first['subnet_prefix'][-2..-1]
      subnet_address = subnet_address + mask
      logger.debug 'Start searching ip address in subnet ' + subnet_address

      unless from_address
        addr = IPAddr.new(subnet_address.to_s)
	      from_address = IPAddr.new((addr.to_range().first.to_i + 1), Socket::AF_INET).to_s
      end
      
      unless to_address
        addr = IPAddr.new(subnet_address.to_s)
        to_address = IPAddr.new((addr.to_range().last.to_i - 1), Socket::AF_INET).to_s
      end
      
      logger.debug 'Starting search for a free ip address form ' + from_address.to_s + ' to ' + to_address.to_s
      possible_ip = free_ips.find_free_ip(from_address, to_address, all_hosts(subnet_address) + all_leases(subnet_address))
      logger.debug 'Possible ip found: ' + possible_ip

      while possible_ip != nil 
        begin
          Resolv.getname possible_ip
          logger.warning 'Address ' + possible_ip + ' resolved. Cannot use it'
          possible_ip = free_ips.find_free_ip(from_address, to_address, all_hosts(subnet_address) + all_leases(subnet_address))
	        rescue Resolv::ResolvError => e
		        logger.warning 'Address ' + possible_ip + ' not resolved. Can use it'
          return possible_ip
        end
      end
    end



    def add_record(options={})
      record = super(options)
      dhcp_kea_network.add_dhcp_record options
      logger.info msg = "Added DHCP record"
    rescue Exception => e
      logger.error msg = "Error adding DHCP record: #{e}"
      raise Proxy::DHCP::Error, msg
    end

    def del_record(record)
      # libvirt only supports one subnet per network
      dhcp_kea_network.del_dhcp_record record
      logger.error msg = "Removed DHCP record: #{e}"
    rescue Exception => e
      logger.error msg = "Error removing DHCP record: #{e}"
      raise Proxy::DHCP::Error, msg
    end
    
    def find_record(subnet_address, ip_or_mac_address)
      if ip_or_mac_address =~ Resolv::IPv4::Regex
        dhcp_kea_network.find_records_by_ip ip_or_mac_address
      else
        dhcp_kea_network.find_records_by_mac ip_or_mac_address
      end
    end

    def find_records_by_ip(subnet_address, ip_address)
      dhcp_kea_network.find_records_by_ip ip_address
    end

    def find_records_by_mac(subnet_address, mac_address)
      dhcp_kea_network.find_records_by_mac mac_address
    end
    
    def del_record_by_mac(subnet_address, mac_address)
      dhcp_kea_network.del_record_by_mac mac_address
    end
  end
end

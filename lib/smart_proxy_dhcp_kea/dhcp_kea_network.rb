require 'mysql2'

module ::Proxy::DHCP::DhcpKea
  class KeaDHCPNetwork

    attr_reader :host, :port, :dbname, :username, :password, :db

    def initialize(host, port, dbname, username, password, free_ips_service = nil, db)
      @host = host
      @port = port
      @db = db
      @dbname = dbname
      @username = username
      @password = password 
      @free_ips = free_ips_service
    end

    def dhcp_leases
      find_network.dhcp_leases
    rescue ArgumentError
      # workaround for ruby-libvirt < 0.6.1 - DHCP leases API is broken there
      # (http://libvirt.org/git/?p=ruby-libvirt.git;a=commit;h=c2d4192ebf28b8030b753b715a72f0cdf725d313)
      []
    end

    def add_dhcp_record(record)
    
      con = Mysql2::Client.new(:dbname => dbname, :user => username, :password => password, :host => host, :port => port)
      st = con.prepare("SELECT subnet_id from dhcp4_subnet WHERE subnet_prefix like ? ORDER BY RIGHT(subnet_prefix,2) DESC;")
      subnet_id = nil
      subnet_id = st.execute(record['network'].to_s + '%')[0]['subnet_id']

      unless subnet_id
           raise Proxy::DHCP::Error, "Unable to find subnet_id for ip: #{record.ip}"
      end

      address = record['nextServer']
      if !!(address =~ Regexp.union(Resolv::IPv4::Regex))
          nextServer = address
      else
          nextServer = Resolv.getaddress(address)
      end

	    st = con.prepare("INSERT INTO hosts(dhcp_identifier,dhcp_identifier_type,dhcp4_subnet_id,ipv4_address,hostname,dhcp4_client_classes,dhcp4_next_server,dhcp4_boot_file_name) VALUES(?,0,?,INET_ATON(?),?,'foreman',INET_ATON(?),?)")
	    st.execute(record['mac'].gsub(":",""), subnet_id, record['ip'], record['name'], record['nextServer'],record['filename']) 
      con.close if con

    end


    def del_dhcp_record(record)

	    con = Mysql2::Client.new(:dbname => dbname, :user => username, :password => password, :host => host, :port => port)
	    st = con.prepare("DELETE FROM hosts WHERE dhcp_identifier = ?")
	    st.execute(record.mac.gsub(":",""))
      con.close if con

    end


    def del_record_by_mac(mac)

      con = Mysql2::Client.new(:dbname => dbname, :user => username, :password => password, :host => host, :port => port)
      st = con.prepare("DELETE FROM hosts WHERE dhcp_identifier = ?")
      st.execute(mac.gsub(":",""))
      con.close if con

    end


    def find_records_by_mac(mac)

      con = Mysql2::Client.new(:dbname => dbname, :user => username, :password => password, :host => host, :port => port)
      st = con.prepare("SELECT dhcp_identifier as mac,(INET_NTOA(ipv4_address)) as ip,hostname FROM hosts WHERE dhcp_identifier = ?")
      host_info = st.execute(mac.gsub(":","")).first
      con.close if con

      Proxy::DHCP::Record.new(host_info[0]['hostname'], host_info[0]['ip'], host_info[0]['mac'].scan(/.{1,2}/).join(':'))

    end

    def find_records_by_ip(ip)
      
      con = Mysql2::Client.new(:dbname => dbname, :user => username, :password => password, :host => host, :port => port)
      st = con.prepare("SELECT dhcp_identifier as mac,INET_NTOA(ipv4_address) as ip,hostname FROM hosts WHERE ipv4_address = INET_ATON(?)")
      host_info = st.execute(ip).first
      con.close if con
      
      Proxy::DHCP::Record.new(host_info[0]['hostname'], host_info[0]['ip'], host_info[0]['mac'].scan(/.{1,2}/).join(':'))
      rescue Exception => e
        return ''
        raise e
      end

    end

  end


module Proxy::DHCP::DhcpKea
  class Plugin < ::Proxy::Provider
    plugin :dhcp_kea, ::Proxy::DHCP::DhcpKea::VERSION

    default_settings :host => '127.0.0.1', :port => '5432', :dbname => 'keadb', :username => 'kea', :db => 'kea'

    requires :dhcp, '>= 1.16'

    validate_readable :config, :leases

    load_classes ::Proxy::DHCP::DhcpKea::PluginConfiguration
    load_dependency_injection_wirings ::Proxy::DHCP::DhcpKea::PluginConfiguration

    start_services :free_ips
  end
end

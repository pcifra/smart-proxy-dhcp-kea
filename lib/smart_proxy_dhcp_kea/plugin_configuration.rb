module Proxy::DHCP::DhcpKea
  class PluginConfiguration
    
    include Proxy::Log	  
	  
    def load_dependency_injection_wirings(container, settings)


      logger.debug 'Plugin initializer, database used is ' + settings[:db].to_s

      container.dependency :memory_store, ::Proxy::MemoryStore
      
      container.dependency :subnet_service, (lambda do
        ::Proxy::DHCP::SubnetService.new(container.get_dependency(:memory_store),
                                         container.get_dependency(:memory_store), container.get_dependency(:memory_store),
                                         container.get_dependency(:memory_store), container.get_dependency(:memory_store))
      end)


      container.dependency :subnet_service_initializer, (lambda do
        ::Proxy::DHCP::DhcpKea::SubnetServiceInitializer.new(settings[:config], settings[:leases],
                                                               container.get_dependency(:parser), container.get_dependency(:subnet_service))
      end)


      container.dependency :dhcp_kea_network, (lambda do
       ::Proxy::DHCP::DhcpKea::KeaDHCPNetwork.new(settings[:host], settings[:port], settings[:dbname], settings[:username], settings[:password], settings[:db])
      end)


      container.dependency :initialized_subnet_service, (lambda do
        ::Proxy::DHCP::DhcpKea::SubnetServiceInitializer.new(settings[:host], settings[:port], settings[:dbname], settings[:username], settings[:password], settings[:db]).initialized_subnet_service(container.get_dependency(:subnet_service))
      end)


      container.singleton_dependency :free_ips, lambda {::Proxy::DHCP::FreeIps.new }

      container.dependency :dhcp_provider, (lambda do
        ::Proxy::DHCP::DhcpKea::Provider.new(settings[:host], settings[:port], settings[:dbname], settings[:username], settings[:password], container.get_dependency(:dhcp_kea_network),
                                           container.get_dependency(:initialized_subnet_service),
                                           container.get_dependency(:free_ips), settings[:db])
      end)


    end

    def load_classes
      require 'smart_proxy_dhcp_kea/dhcp_kea_network'
      require 'dhcp_common/subnet_service'
      require 'dhcp_common/free_ips'
      require 'dhcp_common/server'
      require 'smart_proxy_dhcp_kea/subnet_service_initializer'
      require 'smart_proxy_dhcp_kea/dhcp_kea_main'
    end
  end
end

require 'rexml/document'
require 'ipaddr'
require 'pg'
require 'mysql2'


module Proxy::DHCP::DhcpKea
  class SubnetServiceInitializer
    include Proxy::Log

    attr_reader :host, :port, :dbname, :username, :password, :db

    def initialize(host, port, dbname, username, password, db, free_ips_service = nil)
      @host = host
      @port = port
      @db = db
      @dbname = dbname
      @username = username
      @password = password 
      @free_ips = free_ips_service
    end

    def initialized_subnet_service(subnet_service)
      subnet_service.add_subnets(*parse_config_for_subnets)
      logger.debug msg = "Initializing subnet"
    #  load_subnet_data(subnet_service)
      subnet_service
    end

    def parse_config_for_subnets
      
      ret_val = []

	    con = Mysql2::Client.new(:dbname => dbname, :user => username, :password => password, :host => host, :port => port) 
	    rs = con.query ("SELECT subnet_prefix from dhcp4_subnet")

      require 'ipaddress'
      rs.each do |subnet|
	      ip = IPAddress::IPv4.new subnet['subnet_prefix'] 
        ret_val << Proxy::DHCP::Subnet.new(ip.address, ip.netmask)
      	logger.debug ip.to_s
      end
      logger.debug ret_val.to_s
      ret_val
    rescue Exception => e
      logger.error msg = "Unable to parse subnets: #{e}"
      raise Proxy::DHCP::Error, msg
    end

    # Expects subnet_service to have subnet data
    def parse_config_for_dhcp_reservations(subnet_service)
      to_ret = []
      doc = REXML::Document.new xml = libvirt_network.dump_xml
      REXML::XPath.each(doc, "//network/ip[not(@family) or @family='ipv4']/dhcp/host") do |e|
        subnet = subnet_service.find_subnet(e.attributes['ip'])
        to_ret << Proxy::DHCP::Reservation.new(
            e.attributes["name"],
            e.attributes["ip"],
            e.attributes["mac"],
            subnet,
            :hostname => e.attributes["name"])
      end
      to_ret
    rescue Exception => e
      logger.error msg = "Unable to parse reservations XML: #{e}"
      logger.debug xml if defined?(xml)
      raise Proxy::DHCP::Error, msg
    end

    def load_subnet_data(subnet_service)
      reservations = parse_config_for_dhcp_reservations(subnet_service)
      reservations.each { |record| subnet_service.add_host(record.subnet_address, record) }
      leases = load_leases(subnet_service)
      leases.each { |lease| subnet_service.add_lease(lease.subnet_address, lease) }
    end

    # Expects subnet_service to have subnet data
    def load_leases(subnet_service)
      leases = libvirt_network.dhcp_leases
      leases.map do |element|
        subnet = subnet_service.find_subnet(element['ipaddr'])
        Proxy::DHCP::Lease.new(
            nil,
            element['ipaddr'],
            element['mac'],
            subnet,
            Time.now.utc,
            Time.at(element['expirytime'] || 0).utc,
            'active'
        )
      end
    end
  end
end


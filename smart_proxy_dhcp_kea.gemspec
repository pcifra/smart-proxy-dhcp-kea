require File.expand_path('../lib/smart_proxy_dhcp_kea/version', __FILE__)
require 'date'

Gem::Specification.new do |s|
  s.name        = 'smart_proxy_dhcp_kea'
  s.version     = Proxy::DHCP::DhcpKea::VERSION
  s.date        = Date.today.to_s
  s.license     = 'GPL-3.0'
  s.authors     = ['Pierfrancesco Cifra']
  s.email       = ['pierfrancesco.cifra@cern.ch']
  s.homepage    = 'https://gitlab.cern.ch/pcifra/smart_proxy_dhcp_kea'

  s.summary     = "Dhcp Kea provider plugin for Foreman's smart proxy"
  s.description = "Dhcp Kea provider plugin for Foreman's smart proxy"

  s.files       = Dir['{config,lib,bundler.d}/**/*'] + ['README.md', 'LICENSE']
  s.test_files  = Dir['test/**/*']

  s.add_development_dependency('rake')
  s.add_development_dependency('mocha')
  s.add_development_dependency('test-unit')
  
  s.add_runtime_dependency 'ipaddr'
  s.add_runtime_dependency 'ipaddress'
  s.add_runtime_dependency 'mysql2'
end
